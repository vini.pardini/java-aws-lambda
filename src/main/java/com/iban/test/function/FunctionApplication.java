package com.iban.test.function;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iban.test.function.business.IUserRegistration;
import com.iban.test.function.business.entitiy.UserBO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@SpringBootApplication
@RestController
@Api(value = "User Registration")
public class FunctionApplication  extends SpringBootServletInitializer{
	
	@Autowired
	IUserRegistration iUserRegistration;

	public static void main(String[] args) {
		SpringApplication.run(FunctionApplication.class, args);
	}
	
	@PostMapping("/")
	@ApiOperation(value = "Start the processo of user Registration")
	public ResponseEntity<?> registerUser(@RequestParam(value = "name") String name,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "gender") String gender,
			@RequestParam(value = "dateOfBirth") Date birth,
			@RequestParam(value = "consent") Boolean consent,
			@RequestParam(value = "campaignId") Integer campaignId
			) {
		try {
			UserBO user = new UserBO(email, gender, birth, consent, campaignId);
			
			if(iUserRegistration.validateData(user)) {
				iUserRegistration.registerUser(user);
				return new ResponseEntity<>("{result:OK}", HttpStatus.OK);
			}
		}catch(Exception e) {
			System.out.println("Erro Controller");
			return new ResponseEntity<>("{result:ERROR}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("{result:NOK}", HttpStatus.CONFLICT);
		
	}
}
