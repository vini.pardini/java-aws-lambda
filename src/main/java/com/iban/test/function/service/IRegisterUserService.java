package com.iban.test.function.service;

import com.iban.test.function.business.entitiy.UserBO;

public interface IRegisterUserService {
	
	public String registerUser(UserBO userBO);

}
