package com.iban.test.function.business.entitiy;

import java.util.Date;

public class UserBO {
	
	String email;
	String gender;
	Date birth;
	Boolean consent;
	Integer campaignId;
	
	public UserBO(String email, String gender, Date birth, Boolean consent, Integer campaignId) {
		super();
		this.email = email;
		this.gender = gender;
		this.birth = birth;
		this.consent = consent;
		this.campaignId = campaignId;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	public Boolean getConsent() {
		return consent;
	}
	public void setConsent(Boolean consent) {
		this.consent = consent;
	}
	public Integer getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}
	
	

}
