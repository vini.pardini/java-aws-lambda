package com.iban.test.function.business;

import com.iban.test.function.business.entitiy.UserBO;

public interface IUserRegistration {
	
	public Boolean validateData(UserBO user);
	
	public String registerUser( UserBO user);
	

}
