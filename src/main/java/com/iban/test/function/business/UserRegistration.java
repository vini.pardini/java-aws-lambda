package com.iban.test.function.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iban.test.function.business.entitiy.UserBO;
import com.iban.test.function.service.IRegisterUserService;

@Service
public class UserRegistration implements IUserRegistration {
	
	@Autowired
	IRegisterUserService registerUserservice;

	@Override
	public Boolean validateData(UserBO user) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String registerUser(UserBO user) {
		String result = "error";
		try {
			result = registerUserservice.registerUser(user);
		}catch(Exception e) {
			System.out.println("Error");
		}
		return result;
	}

}
